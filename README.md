# Запуск проекта
### Шаг 1/4
Для того, чтобы успешно запустить проект, необходимо скачать и установить следующие инструменты:
  - Скачать данный репозиторий и распаковать.
  - [JDK 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
  - [Intellij IDEA Ultimate Edition](https://www.jetbrains.com/idea/download/#section=windows)
  - [PostgreSQL](https://www.postgresql.org/)
  - [PG Admin 4](https://www.pgadmin.org/download/pgadmin-4-windows/) <- обычно идёт в комплекте с самой БД

После установки всех компонентов необходимо перезагрузить компьютер.

### Шаг 2/4
Ставим PG Admin 4.
Когда Вас попросят указать Логин и Пароль пользователю в Админке, то указываем следующие данные:
 - Логин - **postgres**
 - Пароль - **postgres**
 
Необходимо будет создать сервер, а затем базу. Серверу выбираем любое имя.
Базу создаём под названием **todo**

### Шаг 3/4
Затем зайти в Intellij IDEA и нажать **New** -> **Project from Version Control**.
В открывшемся диалоговом окне в поле URL вписать ссылку для клонирования данного проекта [https://Acid_Rain909@bitbucket.org/Acid_Rain909/todo-grails.git]()

### Шаг 4/4
Запустите проект нажатием кнопки **Run** или нажмите **Shift + F10**.

Проект загрузится и автоматически откроется страница браузера с приложением.