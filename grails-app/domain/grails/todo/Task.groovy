package grails.todo

import java.time.LocalDate

class Task {

    String name //Название задачи
    Date date//День выполнения
    boolean result //Результат выполнения

    static constraints = {
        name(blank: false)
        date(nullable: false)
        result(nullable: false)
    }

    static mapping = {
        id generator: 'hilo',
                params: [column: 'id', max_lo: 100]
    }
}
