<%--
  Created by IntelliJ IDEA.
  User: Acid_Rain909
  Date: 25.02.2020
  Time: 16:14
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>TODO ${date}</title>
</head>

<body>
<p>TODO list for date ${date}!</p>

<g:each var="task" in="${tasks}" >
    <p>---</p>
    <p>Name: ${task.name}</p>
    <p>Done: ${task.result}</p>

    <a href="${createLink(controller: 'completeTask', action: 'execute', params: [id:task.id])}">Complete task</a>
</g:each>

<a href="${createLink(uri: '/feed/show')}">Go to FEED</a> <br>
<a href="${createLink(uri: '/')}">Go to Main page</a>
</body>
</html>