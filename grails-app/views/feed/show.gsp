<%--
  Created by IntelliJ IDEA.
  User: Acid_Rain909
  Date: 25.02.2020
  Time: 12:47
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Tasks for Today</title>
</head>

<body>
<div id="tasks">
    TODO for Today
    <g:each var="task" in="${tasks}" >
        <p>---</p>
        <p>Name: ${task.name}</p>
        <p>Done: ${task.result}</p>

        <a href="${createLink(controller: 'completeTask', action: 'execute', params: [id:task.id])}">Complete task</a>
    </g:each>
</div>
<br>
<a href="${createLink(uri: '/')}">Go to Main page</a>
</body>
</html>