<%--
  Created by IntelliJ IDEA.
  User: Acid_Rain909
  Date: 25.02.2020
  Time: 14:03
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Completed</title>
</head>

<body>
<p>Task "${task.name}" has been completed!</p>
<a href="${createLink(uri: '/feed/show')}">Go to FEED</a>
</body>
</html>