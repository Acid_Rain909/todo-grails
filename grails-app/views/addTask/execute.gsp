<%--
  Created by IntelliJ IDEA.
  User: Acid_Rain909
  Date: 25.02.2020
  Time: 15:52
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Added!</title>
</head>

<body>
<p>Task "${task.name}" has been ADDED on date ${task.date}!</p>
<br>
<a href="${createLink(uri: '/feed/show')}">Go to FEED</a> <br>
<a href="${createLink(uri: '/')}">Go to Main page</a>
</body>
</html>