<!DOCTYPE html>
<html>
<head>
    <title>Welcome to TODO app</title>
    <style type="text/css">
    .todo-action {
        border: 1px solid gray;
        margin: 10px;
        padding: 5px;
        max-width: 70vw;
    }
    </style>
</head>

<body>
<p>TODO app</p>

<p>Make your choice!</p>
<br>

<div class="todo-action"><a href="${createLink(uri: '/feed/show')}">1. Agenda</a></div>

<div class="todo-action">2. See TODO on certain date

	<g:form controller="showDay" action="execute">
    <g:datePicker id="switchDate" name="switchDate" value="${new Date()}" precision="day"
                  noSelection="['':'-Choose-']" relativeYears="[-10..50]"/><br/>
        <g:submitButton name="submit" value="submit"/>
	</g:form>

</div>


<div class="todo-action">3. Add task

    <g:form controller="addTask" action="execute">
        Task name:  <g:textField name="taskName" value=""/><br/>
        TODO date: <g:datePicker id="taskDate" name="taskDate" value="${new Date()}" precision="day"
                      noSelection="['':'-Choose-']" relativeYears="[-10..50]"/><br/>
        <g:submitButton name="add" value="add"/>
    </g:form>

</div>
</body>
</html>
