package grails.todo

class FeedController {

    static defaultAction = "show"

    TaskService taskService

    /**
     * Метод получает все задание, которые необходимо выполнить сегодня
     */
    def show = {
        def tasks = taskService.feed()
        [tasks : tasks]
    }
}
