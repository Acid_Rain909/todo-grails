package grails.todo

class CompleteTaskController {

    TaskService taskService

/**
 * Метод отмечает, что задание выполнено
 */
    def execute(Integer id) {
        def task = taskService.completeTask(id)
        [task : task]
    }
}
