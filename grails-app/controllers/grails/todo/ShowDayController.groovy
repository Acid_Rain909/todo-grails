package grails.todo

class ShowDayController {

    TaskService taskService

    /**
     * Метод показывает to-do которые необходимо выполнить в определённый день
     */
    def execute(String switchDate) {
        def tasks = taskService.showDayTasks(switchDate)
        [tasks : tasks, date : switchDate]
    }
}
