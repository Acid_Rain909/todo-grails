package grails.todo

import grails.todo.dto.NewTaskDto

class AddTaskController {

    TaskService taskService

    static allowedMethods = [execute: 'POST']

    /**
     * Метод добавляет задачу в to-do на указанную дату
     */
    def execute(String taskName, String taskDate) {

        def task = taskService.addTask(taskName, taskDate)
        [task : task]
    }
}
