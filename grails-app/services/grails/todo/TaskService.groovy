package grails.todo

import grails.todo.dto.NewTaskDto
import grails.transaction.Transactional
import org.apache.commons.lang.time.DateUtils

import java.text.SimpleDateFormat

class TaskService {

    def feed() {
        Date startOfToday = DateUtils.truncate(new Date(), Calendar.DATE);
        Date endOfToday = DateUtils.addMilliseconds(
                DateUtils.addDays(startOfToday, 1), -1);
        return Task.findAllByDateBetween(startOfToday, endOfToday)
    }

    def addTask(String taskName, String taskDate) {

        Date date = new SimpleDateFormat("E MMM d HH:mm:ss zzz yyyy", Locale.ENGLISH).parse(taskDate)

        def task = new Task();
        task.setName(taskName)
        task.setDate(date)
        task.setResult(false)
        task.setVersion(1)

        task.save()

        return task
    }

    def completeTask(Integer id) {
        def task = Task.findById(id)
        task.setResult(true)

        task.save()

        return task
    }

    def showDayTasks(String stringDate){
        Date date = new SimpleDateFormat("E MMM d HH:mm:ss zzz yyyy", Locale.ENGLISH).parse(stringDate)

        Date startOfToday = DateUtils.truncate(date, Calendar.DATE)
        Date endOfToday = DateUtils.addMilliseconds(
                DateUtils.addDays(startOfToday, 1), -1)
        return Task.findAllByDateBetween(startOfToday, endOfToday)
    }
}
